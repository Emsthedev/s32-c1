//Part 2:
/*
Create a simple server and the following routes with their corresponding HTTP methods and responses:
  If the url is http://localhost:8000/, send a response Welcome to Ordering System
  If the url is http://localhost:8000/dashboard, send a response Welcome to your User's Dashboard!
  If the url is http://localhost:8000/products, send a response Here’s our products available
  If the url is http://localhost:8000/addProduct, send a response Add a course to our resources
      - create a mock datebase of products that has these fields: (name, description, price, stocks)
      - use the request_body to add new products.
  If the url is http://localhost:8000/updateProduct, send a response Update a course to our resources
  If the url is http://localhost:8000/archiveProduct, send a response Archive courses to our resources
Test each endpoints in POSTMAN and save the screenshots

*/
let http = require('http');

let products = [
	{
		"name": "Donut",
		"description": "Soft Flavorful donut",
    "price" : "$40",
    "stocks" : "42"
	},
	{
		"name": "Chocolate",
		"description": "Imported from USA",
    "price" : "$90",
    "stocks" : "80"
	},
  {
		"name": "Milk",
		"description": "Fresh Milk from a Cow",
    "price" : "$35",
    "stocks" : "100"
	}
];

let port = 8000

let server = http.createServer(function(request, response) {
	if(request.url == '/' && request.method == 'GET') {
		response.writeHead(200, {'Content-Type': 'application/json'})
		response.end('Welcome to Ordering System')
	} else if(request.url == '/dashboard' && request.method == 'GET') {
		response.writeHead(200, {'Content-Type': 'application/json'})
		response.end("Welcome to your User's Dashboard!")
	} else if(request.url == '/products' && request.method == 'GET') {
		response.writeHead(200, {'Content-Type': 'application/json'})
		response.write("Here’s our products available")
		response.write(JSON.stringify(products))
		response.end()
	} else if(request.url == '/addProduct' && request.method == 'POST') {
		let request_body = ''

		request.on('data', function(data) {
			request_body += data
		})
		request.on('end', function() {
			console.log(typeof request_body)
			request_body = JSON.parse(request_body)

			let newProduct = {
				"name": request_body.name,
				"description": request_body.description,
        "price" : request_body.price,
        "stocks" : request_body.stocks
			}

			products.push(newProduct)
			console.log(products)

			response.writeHead(200, {'Content-Type': 'application/json'})
			response.write(JSON.stringify(newProduct))
			response.end()
		})

	} else if(request.url = "/updateProduct" && request.method == "PUT"){

				response.writeHead(200, {'Content-Type': 'text/plain'});
				response.end('Update a product to our resources');

	} else if(request.url = "/archiveProduct" && request.method == "DELETE"){

					response.writeHead(200, {'Content-Type': 'text/plain'});
					response.end('Archive product to our resources');

	}

});

server.listen(port);

console.log(`Server is running on localhost: ${port}`);