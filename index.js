//Part 1:
/*
Create a readingListActD folder. Inside, create an index.html and index.js file. Test the connection of your js file to the html file by printing 'Hello World' in the console.
1.)
Create a student class sectioning system based 
on their entrance exam score.
If the student average is from 80 and below. Message: 
Your section is Grade 10 Section Ruby,
If the student average is from 81-120. Message: Your section 
is Grade 10 Section Opal,
If the student average is from 121-160. Message: Your section 
is Grade 10 Section Sapphire,
If the student average is from 161-200 to. Message: Your section
 is Grade 10 Section Diamond

Sample output in the console: Your score is (score). 
You will become proceed to Grade 10 (section)

2.) 
Write a JavaScript function that accepts a string 
as a parameter and find the longest word within the string.

Sample Data and output:
Example string: 'Web Development Tutorial'
Expected Output: 'Development'

3.)
Write a JavaScript function to find the first not repeated character.

Sample arguments : 'abacddbec'
Expected output : 'e'

*/
console.log("Hello World");
console.log("");
//1
let getSection = (avg) => {
	if (avg <= 80) {
		console.log(`Your score is ${avg}. 
        You will proceed to Grade 10 Section Ruby.`);
	} else if (avg >= 81 && avg <= 120) {
		console.log(` Your score is ${avg}. 
        You will proceed to Grade 10 Section Opal`);
	} else if (avg >= 121 && avg <= 160) {
		console.log(`Your score is ${avg}. 
        You will proceed to Grade 10 Section Sapphire`);
	} else if(avg >= 161 && avg <= 200) {
		console.log(`Your score is ${avg}. 
        You will proceed to Grade 10 Section Diamond`);
	} else{
        console.log(`Your average grade is higher than 200`);
    }
};

getSection(79);
getSection(80);

getSection(81);
getSection(120);

getSection(121);
getSection(160)

getSection(161);
getSection(200);

getSection(201);

//2
console.log("");
let longestWord = (string) => {
    let stringg = string.split(" ");
    let longest = 0;
    let longest_word = null;
    for (let i = 0; i < stringg.length; i++) {
        if (longest < stringg[i].length) {
            longest = stringg[i].length;
            longest_word = stringg[i];
        }
    }
    return longest_word;
};

let result = longestWord("Web Development tutorial")
console.log(`Longest Word is: ${result}`);




//3
console.log("");
const numOfChar = 256
 
function firstNonRepeating(string)
{
    let arr = new Array(numOfChar)
    for(let i=0;i<numOfChar;i++){
        arr[i] = [0,0];
    }
 
    for (let i=0;i<string.length;i++) {
        arr[string.charCodeAt(i)][0]++;
        arr[string.charCodeAt(i)][1]= i;
    }
 
    let res = Number.MAX_VALUE;
    for (let i = 0; i < numOfChar; i++)
 
        if (arr[i][0] == 1)
            res = Math.min(res, arr[i][1]);
 
    return res;
}
 

 
let string = "abacddbec";
let index = firstNonRepeating(string);
if (index == Number.MAX_VALUE)
    console.log("Characters are all repeating or it is an empty string");
else
console.log("Non-Repeating charachter:  ",string[index]);